Project Requirements:
1. PHP 8
2. themoviedb credentials that should be added in the .env files

Local Setup
1. clone this app locally
2. run composer install
3. add the following env variables: THEMOVIEDB_API_KEY and THEMOVIEDB_BASE_URL
4. run php artisan server command

Implementation details:

This simple application is using themoviedb api to fetch popular movies and movie details.
I used dependency injection to resolve the themoviedb implementation from ioc container.
This will result in fluent api and additionally flexible backend for different possible
different movies api.