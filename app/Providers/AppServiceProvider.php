<?php

namespace App\Providers;

use App\MovieService\Themoviedb;
use App\MovieService\MovieApiContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MovieApiContract::class, function() {
            return new Themoviedb(
                config('services.themoviedb_base_url'),
                config('services.themoviedb_api_key'),
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
