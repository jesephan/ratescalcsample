<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MovieService\MovieApiContract;

class MoviesController extends Controller
{
    public function show($id, MovieApiContract $service)
    {
        $movie = $service->getMovieDetails($id);

        return view('movie-details', compact('movie'));
    }
}
