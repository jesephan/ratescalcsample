<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MovieService\MovieApiContract;

class PopularMoviesController extends Controller
{
    public function index(Request $request, MovieApiContract $service)
    {
        $movies = $service->getPopularMovies($request->page ?: 1);

        return view('popular-movies', compact('movies'));
    }
}
