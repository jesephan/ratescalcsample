<?php

namespace App\MovieService;

interface MovieApiContract
{
    public function getPopularMovies(int $page, ?int $perPage): array;

    public function getMovieDetails(string|int $movieId): array;
}
