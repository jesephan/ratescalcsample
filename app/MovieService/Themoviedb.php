<?php

namespace App\MovieService;

use Illuminate\Support\Facades\Http;

class Themoviedb implements MovieApiContract
{
    private $baseUrl;
    private $apiKey;

    public function __construct(string $baseUrl, string $apiKey)
    {
        $this->baseUrl = $baseUrl;
        $this->apiKey = $apiKey;
    }

    public function getPopularMovies(int $page, ?int $perPage = 100): array
    {
        return $this->send('get', '/3/movie/popular', [
            'page' => $page,
            'perPage' => $perPage
        ])['results'];
    }

    public function getMovieDetails(string|int $movieId): array
    {
        return $this->send('get', '/3/movie/'.$movieId);
    }

    public function send(string $method, string $url, array $params = []): array
    {
        try {
            $endpoint = $this->baseUrl.$url;

            $response = Http::timeout(30)->{$method}(
                $endpoint,
                $params + ['api_key' => $this->apiKey]
            );

            if ($response->successful()) {
                return $response->json();
            }

            $response->throw();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return [];
        }
    }
}
