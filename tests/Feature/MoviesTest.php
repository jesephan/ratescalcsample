<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MoviesTest extends TestCase
{
    public function test_get_popular_movies()
    {
        $results = [
            [
               'adult' => false,
               'backdrop_path' =>'/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg',
               'genre_ids' => [
                    28,
                    12,
                    878
                ],
               'id' => 634649,
               'original_language' =>'en',
               'original_title' =>'Spider-Man => No Way Home',
               'overview' =>'Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.',
               'popularity' => 7700.02,
               'poster_path' =>'/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg',
               'release_date' =>'2021-12-15',
               'title' =>'Spider-Man => No Way Home',
               'video' => false,
               'vote_average' => 8.4,
               'vote_count' => 7867
            ]
        ];

        $fakeResponse = [
           'page' => 1,
           'results' => $results
        ];

        Http::fake([
            config('services.themoviedb_base_url').'/*' => Http::response($fakeResponse, 200),

        ]);

        $response = $this->get('/');

        $response->assertViewHas('movies', $results)
            ->assertStatus(200);
    }

    public function test_get_movie_details()
    {
        $result = [
           'adult' => false,
           'backdrop_path' => '/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg',
           'belongs_to_collection' => [
               'id' => 531241,
               'name' => 'Spider-Man (Avengers) Collection',
               'poster_path' => '/nogV4th2P5QWYvQIMiWHj4CFLU9.jpg',
               'backdrop_path' => '/AvnqpRwlEaYNVL6wzC4RN94EdSd.jpg'
            ],

            'budget' => 200000000,
            'genres' => [
                [
                   'id' => 28,
                   'name' => 'Action'
                ]
            ],
            'homepage' => 'https =>//www.spidermannowayhome.movie',
            'id' => 634649,
            'imdb_id' => 'tt10872600',
            'original_language' => 'en',
            'original_title' => 'Spider-Man => No Way Home',
            'overview' => 'Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.',
            'popularity' => 7700.02,
            'poster_path' => '/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg',
            'production_companies' => [
                [
                'id' => 420,
                'logo_path' => '/hUzeosd33nzE5MCNsZxCGEKTXaQ.png',
                'name' => 'Marvel Studios',
                'origin_country' => 'US'
                ]
            ],
           'production_countries' => [
                [
                'iso_3166_1' => 'US',
                'name' => 'United States of America'
                ]
            ],
            'release_date' => '2021-12-15',
            'revenue' => 1804372547,
            'runtime' => 148,
            'spoken_languages' => [
                [
                'english_name' => 'English',
                'iso_639_1' => 'en',
                'name' => 'English'
                ]
            ],
            'status' => 'Released',
            'tagline' => 'The Multiverse unleashed.',
            'title' => 'Spider-Man => No Way Home',
            'video' => false,
            'vote_average' => 8.4,
            'vote_count' => 7870
        ];

        Http::fake([
            config('services.themoviedb_base_url').'/3/movie/*' => Http::response($result, 200),

        ]);

        $response = $this->get('/movies/634649');

        $response->assertViewHas('movie', $result)
            ->assertStatus(200);
    }
}
